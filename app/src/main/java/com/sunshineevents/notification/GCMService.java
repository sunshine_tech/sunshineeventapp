package com.sunshineevents.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.GcmTaskService;
import com.google.android.gms.gcm.TaskParams;
import com.sunshineevents.MainActivity;
import com.sunshineevents.Model.BeanCreateEventRealm;
import com.sunshineevents.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class GCMService extends GcmTaskService {
    private int year, month, day;
    private Realm realm;
    List<BeanCreateEventRealm> arrayList = new ArrayList<>();
    private Context mContext;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";

    @Override
    public int onRunTask(TaskParams taskParams) {
        Realm.init(getApplicationContext());
        RealmConfiguration config =
                new RealmConfiguration.Builder()
                        .name("test.db")
                        .schemaVersion(1)
                        .deleteRealmIfMigrationNeeded()
                        .build();
        realm = Realm.getInstance(config);


        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<BeanCreateEventRealm> pojoNotifications = realm.where(BeanCreateEventRealm.class).findAll();
                pojoNotifications.load();
                arrayList = realm.copyFromRealm(pojoNotifications);
            }
        });

        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        c.set(year, month, day);

        for (int i = 0; i < arrayList.size(); i++) {
            long reminderDate = arrayList.get(i).getReminder1();
            long reminderDate2 = arrayList.get(i).getReminder2();
            long reminderDate3 = arrayList.get(i).getReminder3();

             if (reminderDate <= c.getTimeInMillis() && reminderDate>0) {

                createNotification(reminderDate,"reminder1",arrayList.get(i).getLocation(),arrayList.get(i).getDate(), arrayList.get(i).getName());
               /* RealmResults<BeanCreateEventRealm> result = realm.where(BeanCreateEventRealm.class).equalTo("reminder1", reminderDate).findAll();
                result.deleteFromRealm(10);*/
                BeanCreateEventRealm toEdit = realm.where(BeanCreateEventRealm.class)
                        .equalTo("reminder1", reminderDate).findFirst();
                realm.beginTransaction();
                toEdit.setReminder1(0);

                realm.commitTransaction();


            }


           else if (reminderDate2 <= c.getTimeInMillis() && reminderDate2>0) {
                 BeanCreateEventRealm toEdit = realm.where(BeanCreateEventRealm.class)
                         .equalTo("reminder2", reminderDate2).findFirst();
                 realm.beginTransaction();
                 toEdit.setReminder2(0);

                 realm.commitTransaction();
                createNotification(reminderDate2,"reminder2",arrayList.get(i).getLocation(),arrayList.get(i).getDate(), arrayList.get(i).getName());
            }


            if (reminderDate3 <= c.getTimeInMillis() && reminderDate3>0) {
                BeanCreateEventRealm toEdit = realm.where(BeanCreateEventRealm.class)
                        .equalTo("reminder3", reminderDate3).findFirst();
                realm.beginTransaction();
                toEdit.setReminder3(0);

                realm.commitTransaction();
                createNotification(reminderDate3,"reminder3",arrayList.get(i).getLocation(),arrayList.get(i).getDate(), arrayList.get(i).getName());
        }
        }
        Log.e(">>>", taskParams.getTag().toString());
        return GcmNetworkManager.RESULT_SUCCESS; }

    public void createNotification(final long datemilli,final String colloumnName, String location, String date, final String eventname)
    {
        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplication().getPackageName() + "/" + R.raw.song);

        int num = (int) System.currentTimeMillis();
        Intent resultIntent = new Intent(getApplicationContext() , MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplicationContext() ,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new NotificationCompat.Builder(getApplicationContext() );
        mBuilder.setSmallIcon(R.drawable.icon_event);
        mBuilder.setContentTitle(eventname)
                .setContentText("Event Date "+date)
                .setSubText("Event Location "+location)
                .setAutoCancel(false)
                .setNumber(1)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) getApplicationContext() .getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setShowBadge(true);

            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(num /* Request Code */, mBuilder.build());
    }
private void mm(){
    Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplication().getPackageName() + "/" + R.raw.song);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {



        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build();

        NotificationChannel Channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                getApplication().getString(R.string.app_name),
                NotificationManager.IMPORTANCE_HIGH);

        // Configure the notification channel.
        Channel.setDescription("msg");
        Channel.enableLights(true);
        Channel.enableVibration(true);
        Channel.setSound(sound, attributes); // This is IMPORTANT


        if (mNotificationManager != null)
            mNotificationManager.createNotificationChannel(Channel);
    }
}
    private void notification(final String eventDate, final String eventName, int i) {
        int num = (int) System.currentTimeMillis();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<BeanCreateEventRealm> result = realm.where(BeanCreateEventRealm.class).equalTo("name", eventName).findAll();
                result.deleteAllFromRealm();
            }
        });

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channel_id")
                .setContentTitle(eventName)
                .setContentText("Event Date " + eventDate)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setSmallIcon(R.drawable.icon_event)
                .setAutoCancel(true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(num, notificationBuilder.build());
    }
    @Override
    public void onInitializeTasks() {
        super.onInitializeTasks();
        // Reschedule removed tasks here
    }

}
