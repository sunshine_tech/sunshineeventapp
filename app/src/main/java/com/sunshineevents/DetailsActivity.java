package com.sunshineevents;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sunshineevents.databinding.ActivityDetailsBinding;

public class DetailsActivity extends AppCompatActivity {
ActivityDetailsBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_details);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_details);
        initView();
    }

    private void initView() {
binding.ivBack.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        finish();
    }
});
        Intent intent=getIntent();
      String FullName=  intent.getStringExtra("icon");
        try {
            int firstSpace = FullName.indexOf(" ");  // detect the first space character

            String image = FullName.substring(0, firstSpace);  // get everything upto the first space character
                String lastName = FullName.substring(firstSpace).trim();
            Glide.with(this).load(image).override(300, 200).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(binding.mImageView);
binding.tvVicinity.setText(lastName);

        }catch (Exception e){

        }

            binding.tvStatus.setText(intent.getStringExtra("status"));

        binding.tvLatitude.setText(intent.getStringExtra("lat"));
        binding.tvLogitude.setText(intent.getStringExtra("lng"));
        binding.tvTitle.setText(intent.getStringExtra("title"));


    }
}
