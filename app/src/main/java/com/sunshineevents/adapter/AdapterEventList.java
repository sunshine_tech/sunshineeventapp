package com.sunshineevents.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sunshineevents.EventDetailActivity;
import com.sunshineevents.EventsListActivity;
import com.sunshineevents.Model.BeanCreateEvents;
import com.sunshineevents.R;

import java.util.ArrayList;

public class AdapterEventList  extends RecyclerView.Adapter<AdapterEventList.Holder> {
    ArrayList<BeanCreateEvents> al_beanCreateEvents;
    Context context;
    LayoutInflater inflater;
    public AdapterEventList(EventsListActivity eventsListActivity, ArrayList<BeanCreateEvents> al_beanCreateEvents) {
this.al_beanCreateEvents=al_beanCreateEvents;
context=eventsListActivity;
inflater=LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view=inflater.inflate(R.layout.design_eventslist,viewGroup,false);

        Holder holder=new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        final BeanCreateEvents events=al_beanCreateEvents.get(i);
        holder.tvEventName.setText(events.getName());
        holder.tvMessage.setText(events.getMessage());
        holder.tvDate.setText(events.getDate());
        holder.tvTime.setText(events.getTime());
        holder.tvVenue.setText(events.getVenue());
        holder.tvLocation.setText(events.getLocation());
        holder.tvReminder.setText(events.getReminder());
        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent inten=new Intent(context, EventDetailActivity.class);
                inten.putExtra("lat",events.getLat());
                inten.putExtra("lng",events.getLng());
                inten.putExtra("title",events.getName());
                context.startActivity(inten);
            }
        });
    }

    @Override
    public int getItemCount() {
        return al_beanCreateEvents.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView tvEventName,tvDate,tvTime,tvMessage,tvVenue,tvLocation,tvReminder;
        CardView card_view;
        public Holder(@NonNull View itemView) {
            super(itemView);

            tvEventName=itemView.findViewById(R.id.tvEventName);
            tvDate=itemView.findViewById(R.id.tvDate);
            tvTime=itemView.findViewById(R.id.tvTime);
            tvMessage=itemView.findViewById(R.id.tvMessage);
            tvVenue=itemView.findViewById(R.id.tvVenue);
            tvLocation=itemView.findViewById(R.id.tvLocation);
            tvReminder=itemView.findViewById(R.id.tvReminder);
            card_view=itemView.findViewById(R.id.card_view);
        }
    }
}
