package com.sunshineevents;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.PeriodicTask;
import com.google.android.gms.gcm.Task;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sunshineevents.Authentication.LoginActivity;
import com.sunshineevents.notification.GCMService;
import com.sunshineevents.utils.CommonUtilities;
import com.sunshineevents.utils.GPSTracker;
import com.sunshineevents.utils.LatLngInterpolator;
import com.sunshineevents.utils.MarkerAnimation;
import com.sunshineevents.utils.SharedPreferenceWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener, GoogleMap.OnMarkerClickListener {
    private GoogleMap mgoogleMap;
    private GPSTracker gpsTracker;
    private LatLng latLngGPS;
    private PlaceAutocompleteFragment currentLocationAutocomplete;
    String strSearchPlacesUrl;
    LatLng placeLatLng;
    private EditText etNearByPlaces;
    private RequestQueue requestQueue;
    private TextView tvGo;
    private NavigationView drawerNavigation;
    private ImageView ivMenu;
    private DrawerLayout drawerLayout;
    private boolean isSelectDrawerItem = false;
    EditText etPlace;
    private GcmNetworkManager mGcmNetworkManage;

    private LocationRequest mLocationRequest;
    private Marker currentLocationMarker;
    private long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(MainActivity.this);
        if(status==ConnectionResult.SUCCESS){
            initView();
        }
        else
            {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }
    }
    private void initView() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mGcmNetworkManage = GcmNetworkManager.getInstance(this);
        Task task = new PeriodicTask.Builder()
                .setService(GCMService.class)
                .setPeriod(20)
                .setTag("service")
                .setUpdateCurrent(true)
                .setRequiredNetwork(Task.NETWORK_STATE_CONNECTED)
                .setFlex(5)
                .setPersisted(true)
                .build();
        mGcmNetworkManage.schedule(task);

        etNearByPlaces = findViewById(R.id.etNearByPlaces);
        tvGo = findViewById(R.id.tvGo);
        requestQueue = Volley.newRequestQueue(this);
        drawerNavigation = findViewById(R.id.drawerNavigation);
        ivMenu = findViewById(R.id.ivMenu);
        drawerLayout = findViewById(R.id.drawerLayout);
        drawerNavigation.setNavigationItemSelectedListener(this);
        setGpsTracker();
        currentlocAutoComplete();
        tvGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etPlace.getText().toString().equals("")) {
                    CommonUtilities.getInstance().Toast(MainActivity.this, "Please search place");
                } else if (etNearByPlaces.getText().toString().equals("")) {
                    CommonUtilities.getInstance().Toast(MainActivity.this, "Please search near by place");
                } else {
                    search("");
                }

            }
        });
        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.openDrawer(GravityCompat.START);
                } else {
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
            }
        });
        View headerview = drawerNavigation.getHeaderView(0);
        TextView tvUserName = (TextView) headerview.findViewById(R.id.tvUserName);

        tvUserName.setText(SharedPreferenceWriter.getInstance(MainActivity.this).getString("UserName"));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mgoogleMap = googleMap;
        googleMap.setMyLocationEnabled(true);
        // Add a marker in Sydney and move the camera
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngGPS, 12));
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latLngGPS.latitude, latLngGPS.longitude))
                .title("My Location"));

        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                Intent intent = new Intent(MainActivity.this, CreateEventsActivity.class);


                intent.putExtra("lat", String.valueOf(marker.getPosition().latitude));
                intent.putExtra("lng", String.valueOf(marker.getPosition().longitude));
                intent.putExtra("venue", marker.getTitle());
                if (isSelectDrawerItem == true) {
                    String location = getAddressFromLatlng();
                    intent.putExtra("location", location);
                } else if (!etPlace.getText().toString().equals("")) {
                    intent.putExtra("location", etPlace.getText().toString());
                } else {
                    String location = getAddressFromLatlng();
                    intent.putExtra("location", location);
                }


                Toast.makeText(MainActivity.this, "" + marker.getTitle().toString(), Toast.LENGTH_SHORT).show();
                startActivity(intent);
                startLocationUpdates();

            }
        });
    }

    private void setGpsTracker() {
        gpsTracker = new GPSTracker(MainActivity.this);
        if (gpsTracker.canGetLocation()) {
            latLngGPS = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
            Toast.makeText(MainActivity.this, "Your Location is - \nLat: " + gpsTracker.getLatitude() + "\nLong: " + gpsTracker.getLongitude(), Toast.LENGTH_SHORT).show();
            gpsTracker.getLatitude();
            gpsTracker.getLongitude();


        } else {
            gpsTracker.showSettingsAlert();
        }
    }

    private void currentlocAutoComplete() {

        currentLocationAutocomplete = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.currentLocationAutocomplete);
        currentLocationAutocomplete.getView().findViewById(R.id.place_autocomplete_search_button).setVisibility(View.GONE);
        currentLocationAutocomplete.getView().findViewById(R.id.place_autocomplete_clear_button).setVisibility(View.GONE);

        etPlace = ((EditText) currentLocationAutocomplete.getView().findViewById(R.id.place_autocomplete_search_input));
        etPlace.setHint("Search Place...");
        etPlace.setTextSize(15.f);
        etPlace.setMaxLines(1);
        etPlace.setHintTextColor(Color.parseColor("#ffffff"));
        etPlace.setTextColor(Color.parseColor("#ffffff"));
        currentLocationAutocomplete.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                mgoogleMap.clear();
                Toast.makeText(MainActivity.this, "" + place.getAddress(), Toast.LENGTH_SHORT).show();
                Log.d("Maps", "Place selected: " + place.getName());
                String locationName = String.valueOf(place.getAddress());
                Geocoder geocoder = new Geocoder(MainActivity.this, Locale.ENGLISH);
                List<Address> addressList = null;
                try {
                    addressList = geocoder.getFromLocationName(locationName, 1);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (addressList != null && addressList.size() > 0) {
                    double lat = (double) (addressList.get(0).getLatitude());
                    double lng = (double) (addressList.get(0).getLongitude());

                    placeLatLng = new LatLng(lat, lng);
                    CameraUpdate update = CameraUpdateFactory.newLatLngZoom(placeLatLng, 12);
                    mgoogleMap.moveCamera(update);
                    mgoogleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(placeLatLng.latitude, placeLatLng.longitude))
                            .title("Your location"));
                }
            }

            @Override
            public void onError(Status status) {
                Log.d("Maps", "An error occurred: " + status);
            }
        });

    }

    public void search(String search) {
        mgoogleMap.clear();
        final ProgressDialog mDialog = new ProgressDialog(MainActivity.this);
        mDialog.setMessage("Loading..");
        mDialog.show();
        if (!search.equals("")) {
            strSearchPlacesUrl = "https://maps.googleapis.com/maps/api/place/search/json?location=" + latLngGPS.latitude + "," + latLngGPS.longitude + "&radius=15000&keyword=" + search.replaceAll(" ", "") + "&sensor=false&key=AIzaSyCbSHtHVGz7riSCpLOrYjCLt7IBrV_NqZs";

        } else {
            strSearchPlacesUrl = "https://maps.googleapis.com/maps/api/place/search/json?location=" + placeLatLng.latitude + "," + placeLatLng.longitude + "&radius=15000&keyword=" + etNearByPlaces.getText().toString().replaceAll(" ", "") + "&sensor=false&key=AIzaSyCbSHtHVGz7riSCpLOrYjCLt7IBrV_NqZs";

        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, strSearchPlacesUrl,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mDialog.dismiss();
                        hideKeyboard(MainActivity.this);
                        //    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                        //    );
                        try {
                            JSONObject mJsonObject = new JSONObject(response);
                            String status = mJsonObject.getString("status");
                            JSONArray array = mJsonObject.getJSONArray("results");

                            for (int i = 0; i < array.length(); i++) {
                                Marker marker = null;
                                JSONObject innerobject = array.getJSONObject(i);
                                String open_now = null;
                                String name = innerobject.getString("name");
                                String vicinity = innerobject.getString("vicinity");


                                String icon = innerobject.getString("icon");

                                try {
                                    JSONObject opening_hours = innerobject.getJSONObject("opening_hours");
                                    JSONObject geometry = innerobject.getJSONObject("geometry");
                                    JSONObject location = geometry.getJSONObject("location");

                                    String open = opening_hours.getString("open_now");
                                    String lat = location.getString("lat");
                                    String lng = location.getString("lng");
                                    double mlat = Double.parseDouble(lat);
                                    double mlng = Double.parseDouble(lng);
                                    // mgoogleMap.addMarker(new MarkerOptions().position(new LatLng(mlat, mlng)).title(name));
                                    LatLng latLng = new LatLng(mlat, mlng);
                                    marker = mgoogleMap.addMarker(new MarkerOptions().position(new LatLng(mlat, mlng)));

                                    if (open.equals("false")) {
                                        marker.setSnippet("CLOSED");
                                    } else {
                                        marker.setSnippet("OPEN NOW");
                                    }
                                    marker.setTitle(name);


                                } catch (Exception e) {
                                }
                                try {
                                    JSONArray arry = innerobject.getJSONArray("photos");

                                    for (int p = 0; p < arry.length(); p++) {
                                        JSONObject ojphoto_reference = arry.getJSONObject(p);
                                        String photo_reference;
                                        photo_reference = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + ojphoto_reference.getString("photo_reference") + "&key=AIzaSyCbSHtHVGz7riSCpLOrYjCLt7IBrV_NqZs";
                                        marker.setTag(photo_reference + " " + vicinity);


                                    }

                                } catch (Exception e) {

                                }

                                //  photos

                            }

                        } catch (JSONException e) {

                            mDialog.dismiss();
                            Toast.makeText(MainActivity.this, "Please check your internet connection and try again later", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(MainActivity.this, "NetworkTimeout", Toast.LENGTH_SHORT).show();

                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(MainActivity.this, "AuthFailureError", Toast.LENGTH_SHORT).show();
                            //TODO
                        } else if (error instanceof ServerError) {
                            Toast.makeText(MainActivity.this, "ServerError", Toast.LENGTH_SHORT).show();
                            //TODO
                        } else if (error instanceof NetworkError) {
                            Toast.makeText(MainActivity.this, "NetworkError", Toast.LENGTH_SHORT).show();
                            //TODO
                        } else if (error instanceof ParseError) {
                            Toast.makeText(MainActivity.this, "ParseError", Toast.LENGTH_SHORT).show();
                            //TODO
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }


        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.home) {
            drawerLayout.closeDrawer(GravityCompat.START);

        } else if (id == R.id.eventList) {

            drawerLayout.closeDrawer(GravityCompat.START);
            startActivity(new Intent(MainActivity.this, EventsListActivity.class));

        } else if (id == R.id.restaurant) {
            isSelectDrawerItem = true;
            drawerLayout.closeDrawer(GravityCompat.START);
            search("restaurants");

        } else if (id == R.id.entertainment) {
            isSelectDrawerItem = true;
            drawerLayout.closeDrawer(GravityCompat.START);

            search("mall");
        } else if (id == R.id.emergency) {
            isSelectDrawerItem = true;
            drawerLayout.closeDrawer(GravityCompat.START);
            search("firestation");
        } else if (id == R.id.Createvent) {
            isSelectDrawerItem = true;
            drawerLayout.closeDrawer(GravityCompat.START);
            startActivity(new Intent(MainActivity.this, CreateEventDrawerActivity.class));
        } else if (id == R.id.contactUs) {
            isSelectDrawerItem = true;
            drawerLayout.closeDrawer(GravityCompat.START);
            startActivity(new Intent(MainActivity.this, ContactUsActivity.class));
        } else if (id == R.id.logout) {
            drawerLayout.closeDrawer(GravityCompat.START);

            SharedPreferenceWriter.getInstance(MainActivity.this).clearPreferenceValues();
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
        return true;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);

        intent.putExtra("lat", marker.getPosition().latitude);
        intent.putExtra("lng", marker.getPosition().longitude);
        intent.putExtra("title", marker.getTitle());
        Toast.makeText(this, "" + marker.getTitle(), Toast.LENGTH_SHORT).show();
        startActivity(intent);


        return true;
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    private String getAddressFromLatlng() {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        String address = null;
        String city = null;

        try {
            addresses = geocoder.getFromLocation(latLngGPS.latitude, latLngGPS.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return city;
    }

    protected void startLocationUpdates() {
      //Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());
    }
    public void onLocationChanged(Location location) {
        // New location has now been determined
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        showMarker(location);
    }
    private void showMarker(@NonNull Location currentLocation) {
        LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        if (currentLocationMarker == null)

            currentLocationMarker = mgoogleMap.addMarker(new MarkerOptions().position(latLng)
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car)));
        else
            MarkerAnimation.animateMarkerToGB(currentLocationMarker, latLng, new LatLngInterpolator.Spherical());
    }


    public void callMarketPlace() {
        try {
//if play services are disabled so it will return request code 1 and result code 0 in OnActivity result.
            startActivityForResult(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id="+ "com.google.android.gms")), 1);
        }
        catch (android.content.ActivityNotFoundException anfe) {
            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);

        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean isEnable = false;

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 10) {
            if (resultCode == 0) {
                if (isEnable){
                    isEnable = false;
                } else {
                    Intent intent = new Intent(); intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", "com.google.android.gms", null); intent.setData(uri);
                    startActivityForResult(intent,1);
                    isEnable = true;

                }
            }
        }
    }

}