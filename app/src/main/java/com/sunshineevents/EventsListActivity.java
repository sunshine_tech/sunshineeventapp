package com.sunshineevents;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sunshineevents.Model.BeanCreateEvents;
import com.sunshineevents.adapter.AdapterEventList;
import com.sunshineevents.databinding.ActivityEventsListBinding;
import com.sunshineevents.utils.CommonUtilities;
import com.sunshineevents.utils.Constants;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class EventsListActivity extends AppCompatActivity {
ActivityEventsListBinding binding;
ProgressDialog progressDialog;
ArrayList<BeanCreateEvents> al_beanCreateEvents=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_list);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_events_list);
        initView();
    }

    private void initView() {
        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
     getEventList();}

    private void getEventList() {
        progressDialog= CommonUtilities.getInstance().showProressBar(EventsListActivity.this,"Please wait..");
// prepare the Request
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, Constants.getEventsList, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        CommonUtilities.getInstance().hideProgressBar(progressDialog);
                        // display response
                        Log.d("Response", response.toString());

                        Iterator<String> panelKeys = response.keys();

                        while (panelKeys.hasNext()) {
                            JSONObject panel = null; // get key from list
                            try {
                                BeanCreateEvents events=new BeanCreateEvents();
                                panel = response.getJSONObject(panelKeys.next());
                                String date = panel.getString("date");
                                String location = panel.getString("location");
                                String message = panel.getString("message");
                                String name = panel.getString("name");
                                String reminder = panel.getString("reminder");
                                String time = panel.getString("time");
                                String lat = panel.getString("lat");
                                String lng = panel.getString("lng");
                                String venue = panel.getString("venue");
                                events.setDate(date);
                                events.setTime(time);
                                events.setLocation(location);
                                events.setMessage(message);
                                events.setName(name);
                                events.setLat(lat);
                                events.setLng(lng);
                                events.setVenue(venue);
                                events.setReminder(reminder);


                                al_beanCreateEvents.add(events);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        Collections.reverse(al_beanCreateEvents);
                        AdapterEventList adapterEventList=new AdapterEventList(EventsListActivity.this,al_beanCreateEvents);
                        LinearLayoutManager mlayoutManager = new LinearLayoutManager(getApplicationContext());
                        mlayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        binding.rvView.setLayoutManager(mlayoutManager);
                        binding.rvView.setAdapter(adapterEventList);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CommonUtilities.getInstance().hideProgressBar(progressDialog);
                        Log.d("Error.Response", "");
                    }
                }
        );

// add it to the RequestQueue
        RequestQueue rQueue = Volley.newRequestQueue(EventsListActivity.this);
        rQueue.add(getRequest);

    }



}
