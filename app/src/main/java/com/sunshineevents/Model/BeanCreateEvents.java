package com.sunshineevents.Model;

public class BeanCreateEvents  {
    public String name;
    public String message;
    public String date;
    public String time;
    public String venue;
    public String location;
    public String reminder;
    public String lat;
    public String lng;

    public BeanCreateEvents() {
    }

    public BeanCreateEvents( String lat,String lng,String name,String message, String date, String time, String venue, String location, String reminder) {
        this.name = name;
        this.date = date;
        this.time = time;
        this.venue = venue;
        this.location = location;
        this.message=message;
        this.reminder = reminder;
        this.lat=lat;
        this.lng=lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getReminder() {
        return reminder;
    }

    public void setReminder(String reminder) {
        this.reminder = reminder;
    }
}
