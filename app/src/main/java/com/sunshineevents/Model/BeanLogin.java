package com.sunshineevents.Model;

public class BeanLogin {
    public String email;
    public String password;

    public BeanLogin() {
    }

    public BeanLogin(String email, String password) {

        this.email = email;
        this.password = password;
    }

}
