package com.sunshineevents.Model;

import io.realm.RealmObject;

public class BeanCreateEventRealm extends RealmObject {
    public String name;
    public String message;
    public String date;
    public String time;
    public String venue;
    public String location;

    public String lat;
    public String lng;
    public long reminder1;
    public long reminder2;
    public long reminder3;

    public BeanCreateEventRealm() {
    }

    public BeanCreateEventRealm(String lat,String lng,String name,String message, String date, String time, String venue, String location, long reminder1,long reminder2,long reminder3) {
        this.name = name;
        this.date = date;
        this.time = time;
        this.venue = venue;
        this.location = location;
        this.message=message;
        this.reminder1 = reminder1;
        this.lat=lat;
        this.lng=lng;
    }

    public long getReminder2() {
        return reminder2;
    }

    public void setReminder2(long reminder2) {
        this.reminder2 = reminder2;
    }

    public long getReminder3() {
        return reminder3;
    }

    public void setReminder3(long reminder3) {
        this.reminder3 = reminder3;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public long getReminder1() {
        return reminder1;
    }

    public void setReminder1(long reminder1) {
        this.reminder1 = reminder1;
    }
}

