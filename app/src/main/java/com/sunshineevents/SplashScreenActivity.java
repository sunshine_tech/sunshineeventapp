package com.sunshineevents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //debug key: AIzaSyBa1G5NWYz0Z2BOIDlATPsTeUd-BZP-1Vw
        //release key: AIzaSyB1G-CvUKJrxEqhlPvq2Y9BYpo6Z-PIrRI
    }
}
