package com.sunshineevents.Authentication;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sunshineevents.Model.BeanSignUp;
import com.sunshineevents.R;
import com.sunshineevents.databinding.ActivitySignUpBinding;
import com.sunshineevents.utils.CommonUtilities;
import com.sunshineevents.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class SignUpActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;
    ActivitySignUpBinding binding;
    BeanSignUp beanSignUp;
    String srtUserId;
    String strEmail = "";
    private DatabaseReference mFirebaseDatabase;
    private boolean isUserExits = false;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_sign_up);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        initView();
        //getUser();

    }

    private void initView() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference();
        srtUserId = mDatabase.push().getKey();
        textWatcher();
binding.ivBack.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        finish();
    }
});

        binding.tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.etFname.getText().toString().equals("")){
                    binding.etFname.setError("Please enter first name");
                }
                else if  (binding.etLname.getText().toString().equals("")){
                    binding.etLname.setError("Please enter last name");
                }
                else if (binding.etEmail.getText().toString().equals("")){
                    binding.etEmail.setError("Please enter email");
                }
                else if (binding.etMobile.getText().toString().equals("")){
                    binding.etMobile.setError("Please enter mobile number");
                }
                else if (binding.etPassword.getText().toString().equals("")){
                    binding.etPassword.setError("Please enter password");
                }
                else {

                    beanSignUp = new BeanSignUp(binding.etFname.getText().toString(), binding.etLname.getText().toString(), binding.etEmail.getText().toString(), binding.etMobile.getText().toString(),binding.etPassword.getText().toString());
                    getUser();
                }

binding.tvLogin.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        finish();
    }
});


        /*      mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
                  @Override
                  public void onDataChange(DataSnapshot dataSnapshot) {
                      // This method is called once with the initial value and again
                      // whenever data at this location is updated.
                      BeanSignUp person2 = dataSnapshot.getValue(BeanSignUp.class);
                      for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                          String name = (String) postSnapshot.child("fname").getValue();
                          String Email = (String) postSnapshot.child("fmail").getValue();

                          //Getting the data from snapshot
                          BeanSignUp person = postSnapshot.getValue(BeanSignUp.class);
                          strEmail= Email;
                          //Adding it to a string
                          String string = "Email "+person.getEmail()+"\nFname: "+person.getFname()+"\nLname:"+person.getLname()+"\nGender:"+person.getPhone();
                      //    lv_list.setText("Name: "+person.getEmail()+"\nFname: "+person.getFname()+"\nLname:"+person.getLname()+"\nGender:"+person.getPhone());
                          //Displaying it on textview
                          Log.d(">>>", "Value is: "+string);


                      }
                      if (strEmail.equals("suni@gmail.com")){
                          Toast.makeText(SignUpActivity.this, "Exist"+strEmail, Toast.LENGTH_SHORT).show();
                      }
                      else {
                          Toast.makeText(SignUpActivity.this, "NOT"+strEmail, Toast.LENGTH_SHORT).show();
                          mDatabase.child("SIGNUP").child(srtUserId).setValue(beanSignUp);
                      }

                  }

                  @Override
                  public void onCancelled(DatabaseError error) {
                      // Failed to read value
                      Log.w(">>>", "Failed to read value.", error.toException());
                  }
              });*/
                //  mDatabase.child("UserSignUp").child(srtUserId).setValue(beanSignUp);
            }
        });



    }
    private void textWatcher() {


        binding.etFname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etFname.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etLname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etLname.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etEmail.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etMobile.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etPassword.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
    private void getUser() {
        progressDialog= CommonUtilities.getInstance().showProressBar(SignUpActivity.this,"Please wait..");

// prepare the Request
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, Constants.UserSignUp, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        CommonUtilities.getInstance().hideProgressBar(progressDialog);
                        // display response
                        Log.d("Response", response.toString());

                        Iterator<String> panelKeys = response.keys();

                        while (panelKeys.hasNext()) {
                            JSONObject panel = null; // get key from list
                            try {
                                panel = response.getJSONObject(panelKeys.next());
                                String email = panel.getString("email");
                                String password = panel.getString("password");
                                if (isUserExits == false) {
                                    if (binding.etEmail.getText().toString().equals(email)) {
                                        isUserExits = true;

                                    }
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        if (isUserExits == false) {
                            mDatabase.child("UserSignUp").child(srtUserId).setValue(beanSignUp);
                            Toast.makeText(SignUpActivity.this, "User Registered Successfully", Toast.LENGTH_SHORT).show();

finish();
                        } else {
                            Toast.makeText(SignUpActivity.this, "User already exist", Toast.LENGTH_SHORT).show();
                            isUserExits=false;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", "");
                    }
                }
        );

// add it to the RequestQueue
        RequestQueue rQueue = Volley.newRequestQueue(SignUpActivity.this);
        rQueue.add(getRequest);

    }
}
