package com.sunshineevents.Authentication;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sunshineevents.MainActivity;
import com.sunshineevents.Model.BeanLogin;
import com.sunshineevents.R;
import com.sunshineevents.databinding.ActivityLoginBinding;
import com.sunshineevents.utils.CommonUtilities;
import com.sunshineevents.utils.Constants;
import com.sunshineevents.utils.SharedPreferenceWriter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;
    private boolean isUserExits = false;
    private DatabaseReference mDatabase;
    String srtUserId;
    BeanLogin beanLogin;
    private ProgressDialog progressDialog;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    LocationManager service;
    boolean enabled;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        service = (LocationManager) getSystemService(LOCATION_SERVICE);
        enabled= service.isProviderEnabled(LocationManager.GPS_PROVIDER);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //

            // Check if enabled and if not redirect user to the GPS settings
           /* if (!enabled) {
                CommonUtilities.getInstance().Toast(LoginActivity.this,"Please Enable GPS");
                showGPSDisabledAlertToUser();
            }else {*/
                if (!SharedPreferenceWriter.getInstance(LoginActivity.this).getString("UserName").equals("")){
                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
                    finish();
                }
                else {
                    initView();
                }
           // }
        } else {
            checkLocationPermission();
        }

     /*   if (!SharedPreferenceWriter.getInstance(LoginActivity.this).getString("UserName").equals("")){
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            finish();
        }
        else {
            initView();
        }*/


    }
    private void initView() {
        mDatabase = FirebaseDatabase.getInstance().getReference();

        srtUserId = mDatabase.push().getKey();
        getdata();
        btnClicks();
        textWatcher();
    }
    private void btnClicks() {
        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(LoginActivity.this,SignUpActivity.class));
            }
        });

        binding.tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (binding.etEmail.getText().toString().equals("")){
                    binding.etEmail.setError("Please enter email");
                }
                else if (binding.etPassword.getText().toString().equals("")){
                    binding.etPassword.setError("Please enter password");
                }
                else {
                    beanLogin=new BeanLogin(binding.etEmail.getText().toString(),binding.etPassword.getText().toString());
                    getUser();
                }

            }
        });


    }

    private void textWatcher() {
        binding.etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etEmail.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etPassword.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void getUser() {
        progressDialog= CommonUtilities.getInstance().showProressBar(LoginActivity.this,"Please wait..");
// prepare the Request
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, Constants.UserSignUp, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        CommonUtilities.getInstance().hideProgressBar(progressDialog);
                        // display response
                        Log.d("Response", response.toString());

                        Iterator<String> panelKeys = response.keys();

                        while (panelKeys.hasNext()) {
                            JSONObject panel = null; // get key from list
                            try {
                                panel = response.getJSONObject(panelKeys.next());
                                String email = panel.getString("email");
                                String password = panel.getString("password");
                                if (isUserExits == false) {
                                    if (binding.etEmail.getText().toString().equals(email) && binding.etPassword.getText().toString().equals(password)) {
                                        isUserExits = true;
                                        String fname = panel.getString("fname");
                                        SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue("UserName",fname);
                                    }
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        if (isUserExits == true) {
                            mDatabase.child("UserLogin").child(srtUserId).setValue(beanLogin);
                            Toast.makeText(LoginActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();
                            isUserExits=false;
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();

                        } else {
                            Toast.makeText(LoginActivity.this, "Bad Credential. Try Again", Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CommonUtilities.getInstance().hideProgressBar(progressDialog);
                        Log.d("Error.Response", "");
                    }
                }
        );

// add it to the RequestQueue
        RequestQueue rQueue = Volley.newRequestQueue(LoginActivity.this);
        rQueue.add(getRequest);

    }
    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            /*if (!enabled) {
                CommonUtilities.getInstance().Toast(LoginActivity.this,"Please Enable GPS");
                showGPSDisabledAlertToUser();
            }else {*/
                if (!SharedPreferenceWriter.getInstance(LoginActivity.this).getString("UserName").equals(""))
                {
                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
                    finish();
                }
                else {
                    initView();
                }
            //}
        }
    }
    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Please Enable GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                finish();
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    private void getdata(){
        String url="https://bazarmenu.com/wp-json/wp/v2/cart/add";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                      //  Log.d("Error.Response", response);
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("Authorization", "Basic Y2tfNDYzYjQ1NTViYTE5ZjljN2JlZDU1YWNhMWRiNWFmZjk1YzljZWNjZjpjc182MGI3MDRhODdlNjc1NTQyYzVjZGJiMGI4MDYzODJlNDg4NDJkMWQx");
                               return params;
            }

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("product_id", "7092");
                params.put("quantity", "2");
                params.put("user_id", "100");

                return params;
            }
        };
        RequestQueue rQueue = Volley.newRequestQueue(LoginActivity.this);
        rQueue.add(postRequest);
    }

  }
