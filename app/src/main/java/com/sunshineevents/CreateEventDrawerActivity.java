package com.sunshineevents;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sunshineevents.Model.BeanCreateEventRealm;
import com.sunshineevents.Model.BeanCreateEvents;
import com.sunshineevents.databinding.ActivityCreateEventDrawerBinding;
import com.sunshineevents.utils.CommonUtilities;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class CreateEventDrawerActivity extends AppCompatActivity {
    private ActivityCreateEventDrawerBinding binding;
    private DatabaseReference mDatabase;
    private String srtUserId;
    private int year, month, day,hour,minute,second;
    private Calendar calendar;
    private Realm realm;
    private List<BeanCreateEventRealm> arrayList=new ArrayList<>();
    private Date reminderDate1;
    private Date reminderDate2;
    private Date reminderDate3;
    private long remindertimeInMilliseconds1;
    private long remindertimeInMilliseconds2;
    private long remindertimeInMilliseconds3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  setContentView(R.layout.activity_create_event_drawer);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_create_event_drawer);
        initView();
    }

    private void initView() {
        Realm.init(getApplicationContext());
        RealmConfiguration config =
                new RealmConfiguration.Builder()
                        .name("test.db")
                        .schemaVersion(1)
                        .deleteRealmIfMigrationNeeded()
                        .build();
        realm = Realm.getInstance(config);

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<BeanCreateEventRealm> pojoNotifications = realm.where(BeanCreateEventRealm.class).findAll();
                pojoNotifications.load();
                arrayList = realm.copyFromRealm(pojoNotifications);
            }
        });

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        minute= calendar.get(Calendar.MINUTE);
        hour= calendar.get(Calendar.HOUR);
        second=calendar.get(Calendar.SECOND);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        srtUserId = mDatabase.push().getKey();


        binding.etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(999);

            }
        });
        binding.etReminder1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //    showDialog(999);
                Reminder1();

            }
        });
        binding.etReminder2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //    showDialog(999);
                Reminder2();

            }
        });
        binding.etReminder3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //    showDialog(999);
                Reminder3();

            }
        });
        binding.etTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CreateEventDrawerActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String AM_PM ;
                        if(selectedHour < 12) {
                            AM_PM = "AM";
                        } else {
                            AM_PM = "PM";
                        }
                        binding.etTime.setText( selectedHour + ":" + selectedMinute+" "+AM_PM);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (binding.etEventName.getText().toString().equals("")){
                    binding.etEventName.setError("Please enter event name");
                }
                else if (binding.etDate.getText().toString().equals("")){
                    binding.etDate.setError("Please enter event date");
                }
                else if (binding.etTime.getText().toString().equals("")){
                    binding.etTime.setError("Please enter event time");
                }else if (binding.etVenue.getText().toString().equals("")){
                    binding.etVenue.setError("Please enter venue");
                }else if (binding.etLocation.getText().toString().equals("")){
                    binding.etLocation.setError("Please enter location");
                }else if (binding.etReminder1.getText().toString().equals("")){
                    binding.etReminder1.setError("Please enter reminder date 1");
                }
                else if (binding.etReminder2.getText().toString().equals("")){
                    binding.etReminder2.setError("Please enter reminder date 2");
                }
                else if (binding.etReminder3.getText().toString().equals("")){
                    binding.etReminder3.setError("Please enter reminder date 3");
                }else {
                    LatLng latLng=getLocationFromAddress(getApplicationContext(),binding.etLocation.getText().toString());
                    BeanCreateEvents beanCreateEvents=new BeanCreateEvents(String.valueOf(latLng.latitude),String.valueOf(latLng.longitude),binding.etEventName.getText().toString(),binding.etMessage.getText().toString(),binding.etDate.getText().toString(),binding.etTime.getText().toString(),binding.etVenue.getText().toString(),binding.etLocation.getText().toString(),binding.etReminder1.getText().toString());
                   mDatabase.child("EventList").child(srtUserId).setValue(beanCreateEvents);

                    realm.beginTransaction();
                    BeanCreateEventRealm pojo = realm.createObject(BeanCreateEventRealm.class);
                    pojo.setName(binding.etEventName.getText().toString());
                    pojo.setDate(binding.etDate.getText().toString());
                    pojo.setReminder1(remindertimeInMilliseconds1);
                    pojo.setReminder2(remindertimeInMilliseconds2);
                    pojo.setReminder3(remindertimeInMilliseconds3);
                    pojo.setLocation(binding.etLocation.getText().toString());
                    realm.copyToRealm(pojo);
                    realm.commitTransaction();
                    binding.etEventName.setText("");
                    binding.etDate.setText("");
                    binding.etTime.setText("");
                    binding.etMessage.setText("");
                    binding.etReminder1.setText("");
                    CommonUtilities.getInstance().Toast(CreateEventDrawerActivity.this,"Event Created Successfully...");
                    finish();
                }

            }
        });

        textWatcher();

    }
    private void textWatcher() {
        binding.etEventName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etEventName.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etMessage.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etTime.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etDate.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etVenue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etVenue.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etLocation.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.etReminder1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.etReminder1.setError(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2+1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {


        binding.etDate. setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year)); }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (realm != null) {
            realm.close();
        }
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null; }
            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude() );
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return p1;
    }
    private void Reminder1(){
        reminderDate1 = new Date();
        final Calendar cal = Calendar.getInstance();
        cal.setTime(reminderDate1);
        new DatePickerDialog(CreateEventDrawerActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override public void onDateSet(DatePicker view,
                                                    int y, int m, int d) {
                        cal.set(Calendar.YEAR, y);
                        cal.set(Calendar.MONTH, m);
                        cal.set(Calendar.DAY_OF_MONTH, d);

                        // now show the time picker
                        new TimePickerDialog(CreateEventDrawerActivity.this,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override public void onTimeSet(TimePicker view,
                                                                    int h, int min) {
                                        cal.set(Calendar.HOUR_OF_DAY, h);
                                        cal.set(Calendar.MINUTE, min);
                                        reminderDate1 = cal.getTime();
                                        binding.etReminder1.setText(String.valueOf(reminderDate1));
                                        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
                                        try {
                                            Date mDate = sdf.parse(String.valueOf(reminderDate1));
                                            remindertimeInMilliseconds1 = mDate.getTime();

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        double pp = cal.getTimeInMillis();
                                    }
                                }, cal.get(Calendar.HOUR_OF_DAY),
                                cal.get(Calendar.MINUTE), true).show();
                    }
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)).show();
    }
    private void Reminder2(){
        reminderDate2 = new Date();
        final Calendar cal = Calendar.getInstance();
        cal.setTime(reminderDate2);
        new DatePickerDialog(CreateEventDrawerActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override public void onDateSet(DatePicker view,
                                                    int y, int m, int d) {
                        cal.set(Calendar.YEAR, y);
                        cal.set(Calendar.MONTH, m);
                        cal.set(Calendar.DAY_OF_MONTH, d);

                        // now show the time picker
                        new TimePickerDialog(CreateEventDrawerActivity.this,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override public void onTimeSet(TimePicker view,
                                                                    int h, int min) {
                                        cal.set(Calendar.HOUR_OF_DAY, h);
                                        cal.set(Calendar.MINUTE, min);
                                        reminderDate2 = cal.getTime();
                                        binding.etReminder2.setText(String.valueOf(reminderDate2));
                                        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
                                        try {
                                            Date mDate = sdf.parse(String.valueOf(reminderDate2));
                                            remindertimeInMilliseconds2 = mDate.getTime();

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        double pp = cal.getTimeInMillis();
                                    }
                                }, cal.get(Calendar.HOUR_OF_DAY),
                                cal.get(Calendar.MINUTE), true).show();
                    }
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)).show();
    }
    private void Reminder3(){
        reminderDate3 = new Date();
        final Calendar cal = Calendar.getInstance();
        cal.setTime(reminderDate3);
        new DatePickerDialog(CreateEventDrawerActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override public void onDateSet(DatePicker view,
                                                    int y, int m, int d) {
                        cal.set(Calendar.YEAR, y);
                        cal.set(Calendar.MONTH, m);
                        cal.set(Calendar.DAY_OF_MONTH, d);

                        // now show the time picker
                        new TimePickerDialog(CreateEventDrawerActivity.this,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override public void onTimeSet(TimePicker view,
                                                                    int h, int min) {
                                        cal.set(Calendar.HOUR_OF_DAY, h);
                                        cal.set(Calendar.MINUTE, min);
                                        reminderDate3 = cal.getTime();
                                        binding.etReminder3.setText(String.valueOf(reminderDate3));
                                        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
                                        try {
                                            Date mDate = sdf.parse(String.valueOf(reminderDate3));
                                            remindertimeInMilliseconds3 = mDate.getTime();

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        double pp = cal.getTimeInMillis();
                                    }
                                }, cal.get(Calendar.HOUR_OF_DAY),
                                cal.get(Calendar.MINUTE), true).show();
                    }
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)).show();
    }


}
