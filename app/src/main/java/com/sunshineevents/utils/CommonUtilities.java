package com.sunshineevents.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class CommonUtilities {
    private static CommonUtilities objInstance;
    public CommonUtilities() {
    }

    public static CommonUtilities getInstance() {
        if (objInstance == null) {
            objInstance = new CommonUtilities();
        }return objInstance;}

    public static boolean isNetworkAvailable(Context context) {

        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mobile_info = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            NetworkInfo wifi_info = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (mobile_info != null) {
                if (mobile_info.isConnectedOrConnecting()
                        || wifi_info.isConnectedOrConnecting()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (wifi_info.isConnectedOrConnecting()) {
                    return true;
                } else {
                    return false;
                }
            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            System.out.println("" + e);
            return false;
        }
    }


    public  void Toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public ProgressDialog showProressBar(Context context, String message) {

        ProgressDialog progressBar = new ProgressDialog(context);
        progressBar.setMessage(message);
        progressBar.setCancelable(false);
        progressBar.show();
        return progressBar;
    }

    public void hideProgressBar(ProgressDialog progressBar) {
        progressBar.dismiss();
    }
}
