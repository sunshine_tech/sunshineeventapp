package com.sunshineevents;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.mapping.OnMapRenderListener;
import com.here.android.mpa.mapping.SupportMapFragment;
import com.here.android.mpa.routing.RouteManager;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.sunshineevents.utils.CommonUtilities;
import com.sunshineevents.utils.GPSTracker;

import java.io.IOException;
import java.util.List;

public class EventDetailActivity extends AppCompatActivity  {
LatLng latLng;
private ImageView ivBack;
    private Map map = null;
    private com.here.android.mpa.mapping.SupportMapFragment mapFragment = null;
    private TextView homeTitle;
    private GPSTracker gpsTracker;
    private GeoCoordinate geoCoordinate,geoCoordinate2;
   private RouteManager rm = new RouteManager();
    private RoutePlan routePlan = new RoutePlan();
    private RouteOptions routeOptions = new RouteOptions();
    com.here.android.mpa.common.Image originImge,detinationImage ;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        initView();
    }

    private void initView() {
        homeTitle=findViewById(R.id.homeTitle);
        Intent intent=getIntent();
        String lat= intent.getStringExtra("lat");
        String lng= intent.getStringExtra("lng");
        String title= intent.getStringExtra("title");
        latLng=new LatLng(Double.parseDouble(lat),Double.parseDouble(lng));
        geoCoordinate2 = new GeoCoordinate(latLng.latitude, latLng.longitude);
        homeTitle.setText(title);
        hereMap();
        btnClicks();
    }

    private void btnClicks() {
        ivBack=findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        }); }

    private SupportMapFragment getSupportMapFragment() {
        return (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapfragment);
    }
    private void hereMap()
    {
        setGpsTracker();
        mapFragment =  getSupportMapFragment();
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                if (error == OnEngineInitListener.Error.NONE) {
                    progressDialog= CommonUtilities.getInstance().showProressBar(EventDetailActivity.this,"Loading Please wait..");
                    // retrieve a reference of the map from the map fragment


                    map = mapFragment.getMap();
                    mapFragment.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            Toast.makeText(EventDetailActivity.this, "djch", Toast.LENGTH_SHORT).show();
                            return false;
                        }
                    });
                    originImge=new com.here.android.mpa.common.Image();

                    try {
                        originImge.setImageResource(R.drawable.location);
                        MapMarker customMarker = new MapMarker(new GeoCoordinate(geoCoordinate.getLatitude(),geoCoordinate.getLongitude(), 0.0),originImge );
                        Map map = new Map();

                        map.addMapObject(customMarker);

                        routePlan.addWaypoint(new GeoCoordinate(geoCoordinate.getLatitude(), geoCoordinate.getLongitude()));
                        routePlan.addWaypoint(new GeoCoordinate(geoCoordinate2.getLatitude() ,geoCoordinate2.getLongitude()));
                        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
                        routeOptions.setRouteType(RouteOptions.Type.FASTEST);

                        routePlan.setRouteOptions(routeOptions);
                        rm.calculateRoute(routePlan,new RouteListener());


                    } catch (IOException e) {
                        e.printStackTrace();
                    }



                } else {
                    System.out.println("ERROR: Cannot initialize Map Fragment");
                }
            }
        });
    }




    private void setGpsTracker() {
        gpsTracker = new GPSTracker(EventDetailActivity.this);
        if (gpsTracker.canGetLocation()) {
            geoCoordinate = new GeoCoordinate(gpsTracker.getLatitude(), gpsTracker.getLongitude());
            Toast.makeText(EventDetailActivity.this, "Your Location is - \nLat: "
                    + gpsTracker.getLatitude() + "\nLong: " + gpsTracker.getLongitude(), Toast.LENGTH_SHORT).show();
            gpsTracker.getLatitude();
            gpsTracker.getLongitude();


        } else {
            gpsTracker.showSettingsAlert();
        }
    }
 OnMapRenderListener listener = new OnMapRenderListener() {
     @Override
     public void onPreDraw() {

     }

     @Override
     public void onPostDraw(boolean b, long l) {

     }


     @Override
     public void onSizeChanged(int i, int i1) {

     }

     @Override
     public void onGraphicsDetached() {

     }

     @Override
     public void onRenderBufferCreated() {

     }
     // ...

};


  private class RouteListener implements RouteManager.Listener {

        // Method defined in Listener
        public void onProgress(int percentage) {
            // Display a message indicating calculation progress
        }

        // Method defined in Listener
        public void onCalculateRouteFinished(RouteManager.Error error, List<RouteResult> routeResult) {
            // If the route was calculated successfully
            if (error == RouteManager.Error.NONE) {
                CommonUtilities.getInstance().hideProgressBar(progressDialog);
                // Render the route on the map
                MapRoute mapRoute = new MapRoute(routeResult.get(0).getRoute());
                map.addMapObject(mapRoute);
                GeoBoundingBox gbb = routeResult.get(0).getRoute().getBoundingBox();
                detinationImage=new com.here.android.mpa.common.Image();


                try {
                    detinationImage.setImageResource(R.drawable.icon_green_location);
                    MapMarker customMarker = new MapMarker(new GeoCoordinate(geoCoordinate2.getLatitude(),geoCoordinate2.getLongitude(), 0.0),detinationImage );
                    map.addMapObject(customMarker);

                    map.setZoomLevel((13.f));
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
            else {
                // Display a message indicating route calculation failure
            }
        }
    }



   /* private class premiumRouteListener implements CoreRouter.Listener {

        // Method defined in Listener
        public void onProgress(int percentage) {
            // Display a message indicating calculation progress
        }

        // Method defined in Listener
        public void onCalculateRouteFinished(List<RouteResult> routeResult, RoutingError error) {
            // If the route was calculated successfully
            if (error == RoutingError.NONE) {
                // Render the route on the map
                MapRoute   mapRoute = new MapRoute(routeResult.get(0).getRoute());
                map.addMapObject(mapRoute);
            }
            else {
                // Display a message indicating route calculation failure
            }
        }
    }*/
}
